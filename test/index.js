var assert = require('chai').assert,
    Cron = require('../index').Cron;
    
describe('Format', function() {
    it('Cron can instantiate', function() {
        var cron = new Cron();
        assert.isTrue(cron !== null, "Instance of cron instantiated.");
    });
    it('formats date as cron expression', function(){
        var cron = new Cron();
        assert.typeOf(cron.getCronString(new Date()), 'string', "Result is string");
    })
});