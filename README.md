CRON-Clock
=========

A small library providing a method to format a Date object as a valid CRON expression with second precision

## Installation

  npm install cron-clock --save

## Usage

  var Cron = require('cron-clock').Cron;
  var cron = new Cron();
  
  console.log(cron.getCronString(new Date()));
  

    getCronString(date, <boolean>) 
    takes an optional boolean argument to force the date portion
    of the expression to randomly select a new format.  Otherwise,
    date portion of the expression remains the same until the date
    passed no longer matches the previously passed date.

  console.log(cron.getCronString(new Date(), true));
  

## Tests

  npm test

## Release History

* 0.1.0 Initial release