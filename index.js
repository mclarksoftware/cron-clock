module.exports = {
    Cron: function(){
        var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

        var lastDate = -1;
        var cronDateCache = "";

        var isLeapYear = function(year) {
            return (year % 400 === 0) || (year % 100 !== 0 && year % 4 === 0);
        }

        var formatTime = function(now) {
            return now.getSeconds() + " " + now.getMinutes() + " " + now.getHours()
        }
        
        var pick = function(list) {
            return list[Math.floor(Math.random() * list.length)];
        }
        
        var getDayOfMonthFormats = function(now) {
            var formats = [
                now.getDate()
            ];
            var lastOfMonth = monthLength[now.getMonth()];
            if (now.getMonth() == 1 && isLeapYear(now.getFullYear())) {
                lastOfMonth += 1;
            }
            if (now.getDate() == lastOfMonth) {
                formats.push("L");
            }
            return formats;
        }
        
        var formatDayOfMonth = function(now) {
            return pick(getDayOfMonthFormats(now));
        }
        
        var getMonthFormats = function(now) {
            var formats = [
                months[now.getMonth()],
                now.getMonth() + 1
            ];
            return formats;
        }
        
        var formatMonth = function(now) {
            return pick(getMonthFormats(now)) ;
        }
        
        var getDayFormats = function(now) {
            var formats = [
                days[now.getDay()],
                now.getDay() + 1
            ];
            var count = 1;
            var working = new Date(now.getTime());
            working.setDate(1);
            while (working.getDate() !== now.getDate()) {
                if (working.getDay() == now.getDay()) {
                    count += 1;
                }
                working.setDate(working.getDate()+1);
            }
            formats.push("" + (now.getDay() + 1) + "#" + count + "");
            
            var last = true;
            working = new Date(now.getTime());
            while (working.getMonth() == now.getMonth()) {
                if (working.getDate() != now.getDate() && working.getDay() == now.getDay()) {
                    last = false;
                }
                working.setDate(working.getDate() + 1);
            }
            if (last) {
                formats.push("" + (now.getDay()+1) + "L");
            }
            
            return formats;
        }
        
        var formatDay = function(now) {
            return pick(getDayFormats(now));
        }

        var formatDate = function(now,change) {
            if (now.getDate() !== lastDate || change === true) {
            lastDate = now.getDate();
            cronDateCache = "";

            if (Math.random() > 0.5) { //DOM=?
                cronDateCache += "?"
                cronDateCache += " " + formatMonth(now);
                cronDateCache += " " + formatDay(now);
                cronDateCache += " " + now.getFullYear();
            } else { //DOW=?
                cronDateCache += formatDayOfMonth(now);
                cronDateCache += " " + formatMonth(now);
                cronDateCache += " ?";
                cronDateCache += " " + now.getFullYear();
            }
            }
            return cronDateCache
        }

        var getCronString = function(now, updateDateFormat) {
            return formatTime(now) + " " + formatDate(now, updateDateFormat);
        }

        return {
            getCronString: getCronString,
            __test__: {
                getDayOfMonthFormats: getDayOfMonthFormats,
                getMonthFormats: getMonthFormats,
                getDayFormats: getDayFormats
            }
        };
    }
}